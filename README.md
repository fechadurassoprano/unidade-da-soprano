# Unidade-da-Soprano

Apresentação Soprano - Unidade de Fechaduras, Cadeados e Ferragens

Alta qualidade, segurança e inovação. Conheça o amplo mix de fechaduras mecânicas, elétricas e digitais, cadeados, ferragens, acessórios, molas de piso e aéreas.

Com mais de 65 anos de história, buscamos desenvolver produtos que façam parte da vida das pessoas, surpreendendo e inovando em cada detalhe. Comercializamos uma ampla linha de produtos através de cinco unidades de negócios: Fechaduras e Ferragens, Materiais Elétricos, Utilidades Térmicas, Componentes para Móveis e México.

Fábrica da Soprano: Fechaduras, Cadeados e Ferragens
Av. das Indústrias, 400 - Distrito Industrial
CEP: 95.178-070, Farroupilha – RS
Telefone: (54) 2109-6464
Website: <a href="https://www.soprano.com.br/produtos/fechaduras-cadeados-e-ferragens">"Unidade Soprano - Fechaduras e Ferragens"</a>
Email: fechaduras@soprano.com.br
